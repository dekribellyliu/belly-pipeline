def call(Map pipelineParams){
    def git = pipelineParams.jsl.org.dbl.Git.new(this)
    def buildConfig = pipelineParams.jsl.org.dbl.BuildConfig.new("development")
    def build = pipelineParams.jsl.org.dbl.Build.new(this, "development")
    def docker = pipelineParams.jsl.org.dbl.Docker.new(this)
    def kubernetes = pipelineParams.jsl.org.dbl.Kubernetes.new(this, "development")
    def slack = pipelineParams.jsl.org.dbl.Slack.new(this)

    def config 
    def set_config = buildConfig.setPipelineConfig(
        service_name: pipelineParams.service_name,
        kube_namespace: pipelineParams.kube_namespace,
        service_branch: pipelineParams.service_branch,
        slack_channel: pipelineParams.slack_channel
    )

    if (pipelineParams.service_name == "auth-service") {
        config = set_config.buildAuth()
    } else if (pipelineParams.service_name == "storage") {
        config = set_config.buildStorage()
    } else {
        config = set_config.buildGeneral()
    }

    currentBuild.displayName = "${config.kube_namespace} - #"+ currentBuild.number
    currentBuild.description = "Executed by " + currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause')[0]['userId']


    def pom = [];

    println(config)

    def slave = label.templatePod(config.service_name)

    podTemplate(
        label: slave,
        containers: containerTemplates(),
        volumes: volumesTemplate()
    )

    {
        node(slave){

            try{
                stage("Pull Code"){
                    git.checkoutServiceRepo(
                        branch: config.service_branch,
                        service: config.service_name
                    )

                    git.checkoutPipelineRepo(scm, config.pipeline_dir)

                    pom = build.getVersionMavenByPom('pom.xml')

                    slack.notifBuildStarted(
                        channel: config.slack_channel,
                        service_name: config.service_name,
                        version: "1.0.0."+currentBuild.number,
                        namespace: config.kube_namespace,
                        environment: "development",
                        build_url: env.BUILD_URL                      
                    )

                
                    sh "ls -lah"
                    sh "ls -lah pipeline"
                    sh "pwd"
                    sh "ls -lah /"
                    

                    // Set Spring Configmap Location
                    // sh "sed -i -e 's/MS_NAME/${config.service_name}/' ${config.bootstrap}"
                    // sh "mv ${config.bootstrap} src/main/resources/"
                    
                }

                stage('Unit Test & Static Code Analysis'){
                  try
                  {
                    build.buildMaven(
                        config: config,
                        pom: pom
                    )

                    slack.sendGood(
                        channel: config.slack_channel,
                        message:"Stage Unit Test & Static Code Analysis Success :heavy_check_mark: \n" +
                                "service: *${config.kube_deployment_name}:1.0.0.${currentBuild.number}* \n"+
                                "namespace: *${config.kube_namespace}*"
                    )
                  } 
                  catch (e){
                    slack.sendDanger(
                      channel: config.slack_channel,
                      message: "*ALERT* Stage Unit Test & Static Code Analysis Failed :rotating_light: \n" +
                               "service: *${config.kube_deployment_name}:1.0.0.${currentBuild.number}* \n" +
                               "namespace: *${config.kube_namespace}*"
                        )
                    }
                }

                stage('Build Docker Image'){
                  try{

                    docker.buildAndPush(
                        config: config,
                        pom: pom
                    )

                    slack.sendGood(
                        channel: config.slack_channel,
                        message:"Stage Build & Push Docker Image Success :heavy_check_mark: \n" +
                                "service: *${config.kube_deployment_name}:1.0.0.${currentBuild.number}* \n"+
                                "namespace: *${config.kube_namespace}*"
                    )
                  }
                  catch (e){
                    slack.sendDanger(
                      channel: config.slack_channel,
                      message: "*ALERT* Stage Build & Push Docker Image Failed :rotating_light: \n" +
                               "service: *${config.kube_deployment_name}:1.0.0.${currentBuild.number}* \n" +
                               "namespace: *${config.kube_namespace}*"
                        )
                      
                  }
                }

                stage("Deploy to Development"){
                    kubernetes.deploymentBelly(
                        config: config
                    )
                }


            }
            catch (e) {
              slack.notifBuildFailed(
                  environment: 'development',
                  channel: config.slack_channel,
                  service_name: config.service_name,
                  version: "1.0.0."+currentBuild.number,
                  namespace: config.kube_namespace,
                  build_url: env.BUILD_URL
              ) 
                sh "echo error"
                throw e

            }

        }


    }



}