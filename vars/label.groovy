def templatePod(String name="template-pods"){
   return "${name}" 
}

def getShellOut(String shell){
   return sh (
      script: shell,
      returnStdout: true
   ).trim()
}