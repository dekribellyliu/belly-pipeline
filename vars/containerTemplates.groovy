def call() {
    return [
        containerTemplate(
            name: "kubectl",
            image: "gcr.io/cloud-builders/kubectl",
            command: "cat",
            ttyEnabled: "true"),

        containerTemplate(
            name: "docker",
            image: "gcr.io/cloud-builders/docker",
            command: "cat",
            ttyEnabled: "true"),

        containerTemplate(
            name: "maven",
            image: "maven:3.6-jdk-8",
            command: "cat",
            ttyEnabled: "true")
        
    ]
}