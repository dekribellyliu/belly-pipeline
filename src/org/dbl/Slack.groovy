package org.dbl

class Slack implements Serializable {

    private final def script

    private def channel_log_notif = 'belly-jenkins'

    private def jenkins_bot_token = 'investree-bot-slack'


    Slack(def script){
        this.script = script
    }


  /**
  params:
    service_name: String
    version: String
    namespace: String   
    environment: String (development|staging|production)     
    filePath: String
    build_url: env.BUILD_URL
  */
  void uploadSlack(Map args){
    def initialComment = "Log *${args.service_name}:${args.version}* on ${args.environment}\n" +
      " namespace: *${args.namespace}*\n" +
      " `${args.build_url}`"

    script.slackUploadFile(
      channel: this.channel_log_notif,
      filePath: args.filePath,
      credentialId: this.jenkins_bot_token,
      initialComment: initialComment
    )
  }

    /**
    params:
        channel: String
        service_name: String
        version: String
        namespace: String
        environment: String (development|staging|production)
        build_url: env.BUILD_URL
    */

    void notifBuildStarted(Map args){
        def messageNotif = ((args.environment == "production") ? "deploy" : "Pipeline started" ) +
          " *${args.service_name}:${args.version}* on ${args.environment}\n " +
          "namespace: *${args.namespace}*\n" +
          ((args.environment == "production") ? "need approval\n" : "") +
          "`${args.build_url}`"

          script.slackSend(
              channel: args.channel,
              color: "good",
              message: messageNotif
          )
    }

    void notifBuildFailed(Map args){
        def messageNotif = ((args.environment == "production") ? "deploy" : "*ALERT* Pipeline" ) +
          " *${args.service_name}:${args.version}* failed on ${args.environment} :rotating_light:\n" +
          "namespace: *${args.namespace}*\n" +
          "`${args.build_url}`"

        script.slackSend(
            channel: args.channel,
            color: "danger",
            message: messageNotif
        )
    }

    /**
    params:
        channel: String
        service_name: String
        version: String
        namespace: String
        environment: String (development|staging|production)
    */

    void notifWaitDeployKubernetes(Map args){
      def messageNotif = "*${args.service_name}:${args.version}* on process deploying to cluster ${args.environment}\n " +
        "namespace: *${args.namespace}*\n" +
        "please wait max 10 minutes"

        script.slackSend(
            channel: args.channel,
            color: "good",
            message: messageNotif
        )
    }

    /**
    params:
        channel: String
        service_name: String
        version: String
        namespace: String
        environment: String (development|staging|production)
    */

    void notifDeployedKubernetes(Map args){
      def messageNotif = "${args.service_name}:${args.version} has been deployed to cluster ${args.environment}\n " +
        "namespace: ${args.namespace}\n" +
        "${args.service_url}"
      
      script.slackSend(
          channel: args.channel,
          color: "good",
          message: messageNotif
      )
    }

    /**
    params:
      service_name: String
      version: String
      namespace: String
      environment: String (development|staging|production)
      build_url: env.BUILD_URL
      pod_log: String
    */

    void sendLogToSlack(Map args){
      def messageNotif = "*LOG ${args.service_name}:${args.version}* `|` namespace: *${args.namespace}* `|` environment: *${args.environment}* \n" +
        "`${args.build_url}` \n" +
        "${args.pod_log}"

      script.slackSend(
        channel: this.channel_log,
        color: "danger",
        message: messageNotif
      )
    }
    
    /**
    params:
      channel: String
      status_build: String(ABORTED|FAILED)
      message: String
    */

    void slackAndStopBuild(Map args){
      script.slackSend(
        channel: args.channel,
        color: "danger",
        message: args.message
      )
      script.currentBuild.result = args.status_build
      script.error (args.message)
    }

    void sendGood(Map args){
      script.slackSend(
        channel: args.channel,
        color: "good",
        message: args.message
      )
    }

    void sendAttachments(Map args){
      script.slackSend(
        channel: args.channel,
        color: "good",
        attachments: args.attachments
      )
    }

    void sendDanger(Map args){
      script.slackSend(
        channel: args.channel,
        color: "danger",
        tokenCredentialId: 'bot-slack-log-notif',
        botUser: true,
        message: args.message,
        attachments: args.attachments
      )
    }



}