package org.dbl

class Git implements Serializable {

    def gitConfig = [
        credentialsId: "bitbucket",
        email: "dekri@investree.id",
        user: "dekribellyliu"
    ]

    private final def script

    Git(def script){
        this.script = script
    }

        /**
    params:
        service: String
        branch: String
    */

    void checkoutServiceRepo(Map args){
        script.checkout([
            $class: "GitSCM",
            branches: [[name: "*/${args.branch}"]],
            userRemoteConfigs: [[credentialsId: gitConfig['credentialsId'], url:"git@bitbucket.org:dekribellyliu/${args.service}"]]
        ])
    }


    void checkoutPipelineRepo(scm, String directory = 'pipeline') {
        script.dir(directory){
            script.checkout([
                $class: "GitSCM",
                branches: scm.branches,
                userRemoteConfigs: scm.userRemoteConfigs
            ])
        }
    }

    def getVersionTag(String major_version = "v1.0"){

        def minor_version = 1;

        script.sshagent (credentials: [gitConfig['credentialsId']]) 
        
        {
            try {

                def last_tag = script.sh( script: "git for-each-ref --sort=-taggerdate --format '%(tag)' refs/tags/${major_version}.* | grep -v hotfix | head -1", returnStdout: true ).trim()

                def last_version = (last_tag =~ /\d*$/).findAll()

                minor_version = last_version.last().toInteger() + 1
            } 
            catch(e) {
                minor_version = 1;
            }
        }

        return "${major_version}.${minor_version}"
    }

    void createTagAndPush(String tag_version, String comment){

        script.sshagent (credentials: [gitConfig['credentialsId']]) {

            setConfigGit()

            script.sh """  git tag -a ${tag_version} -m "${comment}"  """

            script.sh "git push origin ${tag_version}"

        }

    }

    void setConfigGit() {

        script.sh "git config --global push.followTags true"

        script.sh """ git config --global user.email  "${gitConfig['email']}";  """

        script.sh """ git config --global user.name "${gitConfig['user']}"; """

    }
  

}