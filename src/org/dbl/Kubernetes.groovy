package org.dbl

class Kubernetes implements Serializable {
    private final def script
    private def slack

    private def env

    /**
    params:
        script: Object self
        env: String [development, staging, production]
    */

    Kubernetes(def script, String env = "development") {

        this.script = script
        this.env = env
        this.slack = new Slack(script)

    }

    def sendLogToSlack(config) {
    //   def config = args.configs
      try {
        script.timeout(time: 2, unit: 'MINUTES'){
        def new_pod = script.sh ( 
            script: "kubectl get pods -n ${config.kube_namespace} -l app=${config.kube_deployment_name} --sort-by=.metadata.creationTimestamp -o jsonpath='{.items[*].metadata.name}' | awk '{print \$NF}'", returnStdout: true ).trim()
                            
            script.sh "kubectl -n ${config.kube_namespace} logs ${new_pod} -f > ${config.kube_deployment_name}-${config.kube_namespace}-${script.currentBuild.number}.log "

            // Upload log to slack
            slack.uploadSlack(
            service_name: config.service_name,
            version: "1.0.0.${script.currentBuild.number}",
            namespace: config.kube_namespace,
            environment: this.env,
            filePath: "${config.kube_deployment_name}-${config.kube_namespace}-${script.currentBuild.number}.log",
            build_url: "${script.env.BUILD_URL}"
            // filePath: "file.log"
        )
        }
      } 
      catch(f){
        script.sh "echo fail send pod logs to slack"
      }
    }

    def deploymentBelly(Map args) {
        def config = args.config
        // def pom = args.pom

        script.container('kubectl') {
            script.withKubeConfig([
                credentialsId: "${config.kube_cred_id}",
                serverUrl: "${config.kube_api_server}"
            ]) {
                // Set kube deployment belly
                
                script.sh "sed -i -e 's/MS_NAMESPACE/${config.kube_namespace}/' ${config.kube_deployment_belly} "
                script.sh "sed -i -e 's/MS_NAME/${config.service_name}/' ${config.kube_deployment_belly} "
                script.sh "sed -i -e 's/MS_DEPLOYMENT/${config.kube_deployment_name}/' ${config.kube_deployment_belly} "
                script.sh "sed -i -e 's/MS_SET_REPLICA/${config.kube_set_replica}/' ${config.kube_deployment_belly} "
                
                script.sh "kubectl apply -f ${config.kube_deployment_belly}"

                try{
                    script.timeout(time: 25, unit: 'SECONDS'){
                        slack.notifWaitDeployKubernetes(
                            environment: this.env,
                            channel: config.slack_channel,
                            service_name: config.service_name,
                            version: "1.0.0.${script.currentBuild.number}",
                            namespace: config.kube_namespace
                        )

                    script.sh "kubectl -n ${config.kube_namespace} rollout status deployment/${config.kube_deployment_name} " 

                    slack.notifDeployedKubernetes(
                            environment: this.env,
                            channel: config.slack_channel,
                            service_name: config.service_name,
                            version: "1.0.0.${script.currentBuild.number}",
                            namespace: config.kube_namespace,
                            service_url: config.service_url
                        )
                    } 

                }catch (e){

                        sendLogToSlack(
                            // configs: config
                        )                         
                        

                        script.sh "pwd && ls -lah"

                        
                
                        // Rollback deployment
                        // script.sh "kubectl rollout undo deployment/${config.kube_deployment_name} -n ${config.kube_namespace}"

                        // Delete deployment
                        script.sh "kubectl delete deployment/${config.kube_deployment_name} -n ${config.kube_namespace}"
                            
                        script.sh "kubectl delete svc/${config.service_name}-svc -n ${config.kube_namespace}"

                        script.sh "echo Deployment is failed"

                        this.slack.slackAndStopBuild(
                        channel: config.slack_channel,
                        message: "build ${config.service_name}:1.0.0.${script.currentBuild.number} failed to deploy on cluster ${this.env} \n" +
                        "namespace: ${config.kube_namespace} \n" +
                        "will be rollback to previous version \n" + 
                        "*Please check the log at channel #log-notification*",
                        status_build: "FAILED"
                        )

                        throw e

                }



            }

        }


    }





    def deployment(Map args) {

        def config = args.config
        def pom = args.pom
        script.container('kubectl') {
            script.withKubeConfig([
                credentialsId: "${config.kube_cred_id}",
                serverUrl: "${config.kube_api_server}"
            ]){

                // Set Configmap Environment Variable
                script.sh "sed -i -e 's/MS_NAMESPACE/${config.kube_namespace}/' ${config.env_configmap}"

                if (this.env == "development") {
                    script.sh "sed -i -e 's/TRIBE_NAME/${config.kube_namespace}/' ${config.env_configmap}"
                }

                script.sh "kubectl apply -f ${config.env_configmap}"

                try {
                    // create rollback Configmap Microservice

                    script.sh "kubectl -n ${config.kube_namespace} apply view-last-applied configmap/${config.service_name} > ${config.service_name}-configmap.yaml "

                } catch (e) {
                    script.sh "rm -rf ${config.service_name}-configmap.yaml"
                    script.sh "echo ${e.getMessage()}"

                }

                // Set Configmap Microservice
                script.sh "sed -i -e 's/MS_NAMESPACE/${config.kube_namespace}/' ${config.service_configmap}"
                script.sh "sed -i -e 's/MS_NAME/${config.service_name}/' ${config.service_configmap}"
                script.sh "kubectl apply -f ${config.service_configmap}"

                // Set Deployment Microservice
                script.sh "sed -i '/service: MS_NAME-svc:8080/r ${config.service_cors}' ${config.kube_deployment_file}"
                script.sh "sed -i -e 's/MS_SET_REPLICA/${config.kube_set_replica}/' ${config.kube_deployment_file} "
                script.sh "sed -i -e 's/MS_DEPLOYMENT/${config.kube_deployment_name}/' ${config.kube_deployment_file} "
                script.sh "sed -i -e 's/MS_NAMESPCE/${config.kube_namespace}'/ ${config.kube_deployment_file} "
                script.sh "sed -i -e 's/MS_VERSION/${pom.project_version}/' ${config.kube_deployment_file} "
                script.sh "sed -i -e 's/MS_NAME/${config.service_name}/' ${config.kube_deployment_file} "
                script.sh "sed -i -e 's!DOCKER_IMAGE!${config.docker_image}!' ${config.kube_deployment_file} "
    
                def check_image = script.label.getShellOut("kubectl -n ${config.kube_namespace} get deployment/${config.kube_deployment_name} -o yaml | grep -c ${config.docker_image}:${pom.project_version} || true")

                if (check_image == "0"){
                    script.sh "kubectl apply -f ${config.kube_deployment_file}"
                } else {
                    script.sh "sed -i -e 's!MS_NAMESPACE!${config.kube_namespace}!' ${config.pipeline_dir}/force-deploy.sh"
                    script.sh "sed -i -e 's/MS_DEPLOYMENT/${config.kube_deployment_name}/' ${config.pipeline_dir}/force-deploy.sh "
                    script.sh "sh ${config.pipeline_dir}/force-deploy.sh"
                }

                try {
                    // script.timeout(time: 10, unit: 'MINUTES'){
                        // slack.notifWaitDeployKubernetes(
                        //     environment: this.env,
                        //     channel: config.slack_channel,
                        //     service_name: config.service_name,
                        //     version: pom.project_version,
                        //     namespace: config.kube_namespace
                        // )

                        //checking rollout status
                        script.sh "kubectl -n ${config.kube_namespace} rollout status deployment.v1.apps/${config.kube_deployment_name} "

                        // slack.notifDeployedKubernetes(
                        //     environment: this.env,
                        //     channel: config.slack_channel,
                        //     service_name: config.service_name,
                        //     version: pom.project_version,
                        //     namespace: config.kube_namespace,
                        //     service_url: config.service_url
                        // )


                    // }

                } catch (e){
                    // Get the pod logs before deployment deleted
                    def new_pod = script.label.getShellOut("kubectl get pods -n ${config.kube_namespace} -l app=${config.kube_deployment_name} --sort-by=.metadata.creationTimestamp -o jsonpath='{.items[*].metadata.name}' | awk '{print \$NF}' ")
                    def pod_log = script.label.getShellOut("kubectl -n ${config.kube_namespace} logs ${new_pod}")

                    // Send log to Slack channel #log-notification
                    slack.sendLogToSlack(
                      service_name: config.service_name,
                      version: pom.project_version,
                      namespace: config.kube_namespace,
                      environment: this.env,
                      build_url: "${script.env.BUILD_URL}",
                      pod_log: "${pod_log}"
                    )
                    

                    // Undo deployment and delete it
                    def is_exists = script.sh(
                        script: "if [ -s investree-app-configmap.yaml ]; then echo 'true'; else echo 'false'; fi", returnStdout: true).trim()
                    
                    if (is_exists == "true"){
                        //rollback configmap
                        script.sh "kubectl apply -f ${config.service_name}-configmap.yaml"

                        // rollback deployment
                        script.sh "kubectl -n ${config.kube_namespace} rollout undo deployment.v1.apps/${config.kube_deployment_name}"
                        
                    } else {
                        // delete configmap
                        script.sh "kubectl -n ${config.kube_namespace} delete configmap/${config.service_name}"

                        // delete service
                        script.sh "kubectl -n ${config.kube_namespace}  delete service/${config.service_name}-svc"

                        // delete deployment
                        script.sh "kubectl -n ${config.kube_namespace} delete deployment/${config.kube_deployment_name}"

                    }

                    slack.slackAndStopBuild(
                        channel: config.slack_channel,
                        message: "build ${config.service_name}:${pom.project_version} failed to deploy on cluster ${this.env} \n" +
                          "namespace: ${config.kube_namespace} \n" +
                          "will be rollback to previous version \n" + 
                          "*Please check the log at channel #log-notification*",
                        status_build: "FAILED"
                    )
                   
                    
                }             
            }
        }
    }
}