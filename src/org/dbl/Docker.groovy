package org.dbl

class Docker implements Serializable {
    

    private final def script
    private def urlRepository

    /**
    params:
        script: Object self
    */

    Docker(def script) {
        this.script = script
        // this.urlRepository = "https://registry-intl-vpc.ap-southeast-5.aliyuncs.com"
        this.urlRepository = "https://registry-intl.ap-southeast-5.aliyuncs.com"
    }

    def buildAndPush(Map args){
        def config = args.config
        def pom = args.pom
        script.container('docker'){
            script.withDockerRegistry([
                credentialsId: "${config.docker_registry_cred_id}",
                url: this.urlRepository
            ]){
                // script.sh "sed -i -e 's!PROJECT_PIPELINE_DIR!${config.project_pipeline_dir}!' ${config.dockerfile}"
                // script.sh "docker build --rm -t ${config.docker_image}:${pom.project_version} -f ${config.dockerfile} --no-cache . "
                // script.sh " docker push ${config.docker_image}:${pom.project_version} "
                // script.sh "docker images -a ${config.docker_image}:${pom.project_version} -q" //Show images ID
                // script.sh "docker rmi -f \$(docker images -a ${config.docker_image}:${pom.project_version} -q)" //Remove images

                // script.sh "docker tag nginx:latest ${config.docker_image}:${pom.project_version} "
                // script.sh "docker push ${config.docker_image}:${pom.project_version}"
                // script.sh "docker rmi -f \$(docker images -a ${config.docker_image}:${pom.project_version} -q)"
                script.sh "echo Build Docker Image and Push To Registry"
                script.sh "docker images"
            }
        }
    }


    def buildNginxProxy(Map args){
        def config = args.config
        def pom = args.pom
        script.container('docker'){
            script.withDockerRegistry([
                credentialsId: "${config.docker_registry_cred_id}",
                url: this.urlRepository
            ]){
                script.sh "docker build --rm -t ${config.docker_image}:${pom.project_version} -f ${config.dockerfile} --no-cache . "
                script.sh "docker push ${config.docker_image}:${pom.project_version}"
                script.sh "docker images -a ${config.docker_image}:${pom.project_version} -q"
                script.sh "docker rmi -f \$(docker images -a ${config.docker_image}:${pom.project_version} -q)"    
            }
        }
    }

}