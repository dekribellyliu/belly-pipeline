package org.dbl

class Build implements Serializable {

    private final def script
    private def git
    private def env
    // private def slack
    

    

    /**
     params:
        script: Object self
        env: String [development, staging, production]
    */

    Build(def script, String env = "development") {
        this.script = script
        this.env = env
        this.git = new Git(script)
        // this.slack = new Slack(script)
        
    }

    void setBuildDescription(Map args) {
        script.currentBuild.displayName = args.tittle
        script.currentBuild.description = args.description
    }

  /**
  param: file_path
  return: [
    project_version String
    project_name String
    group_id String
  ]
  */
    
    def getVersionMavenByPom(String file_path = "pom.xml"){
        
        def pom = script.readMavenPom file: file_path
        
        def raw_version = pom.version.replace("-SNAPSHOT", "")

        return [
            project_version: (this.env == "staging") ? git.getVersionTag("v${raw_version}") : "${raw_version}.${script.currentBuild.number}",
            
            project_name: pom.name,
            
            group_id: pom.groupId
            
        ]
    }

  /**
  params:
    config: Build.getConfig
    pom: Build.getVersionMavenByPom
  */
    
    void buildMaven(Map args) {
        def config = args.config
        def pom = args.pom
        script.container('maven'){
            // script.withSonarQubeEnv('sonar-investree'){
                // script.sh 'rm -rf /root/.m2/repository/org/srpingframework/boot/spring-boot-starter-parent/*'
                // script.sh 'mvn clean test'
                // script.sh 'rm -rf src/main/resources/application.yml'
                // script.sh 'rm -rf src/main/resources/parameters.yaml'
                // script.sh 'mvn -e clean package -DskipTests sonar:sonar'
                script.sh 'ls -lah'
                script.sh 'pwd'
                script.sh "echo ${pom}"
            // }
        }
    }

    // script.timeout(time: 15, unit: 'MINUTES'){
    //     def qg = script.waitForQualityGate()
    //     if (qg.status != "OK"){
    //         slack.sendDanger(
    //             channel: config.slack_channel,
    //             message: "https://sonar.investree.id:9999/dashboard?id=${pom.group_id}:${pom.project_name}"
    //         )
    //         slack.slackAndStopBuild(
    //             channel: config.slack_channel,
    //             message: "build ${config.service_name}:${pom.project_version} failed to pass code analysis",
    //             status_build: "ABORTED"
    //         )
    //     }
    // }



}