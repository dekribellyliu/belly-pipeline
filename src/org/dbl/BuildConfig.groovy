package org.dbl

class BuildConfig implements Serializable {

    private def env

    private def envAbbrev = [
        development : "dev",
        staging : "stg",
        production: "prd"
    ]

    private def kubeCredential = [
        development : [ kube_cred_id : "development02-kubernetes", kube_api_server: "https://10.152.183.1" ],
        staging: [ kube_cred_id: "staging02-kubernetes", kube_api_server: "https://10.152.183.1"],
        production: [ kube_cred_id: "production02-kubernetes", kube_api_server: "https://10.152.183.1"]
    ]

    /**
    params:
        script: Object self
        env: String [development, staging, production]
    */

    BuildConfig(String env = "development") {
        this.env = env
    }

    private def service_name
    private def kube_namespace
    private def service_branch
    private def version_tag
    private def slack_channel

    def setPipelineConfig(Map args) {
        this.service_name = args.service_name
        this.kube_namespace = args.kube_namespace ?: "default"
        this.service_branch = args.service_branch ?: "master"
        this.version_tag = args.version_tag ?: "empty_version_tag"
        this.slack_channel = args.slack_channel ?: "belly-jenkins"

        return this
    }

    private def baseMsPipeline() {
        def pipeline_dir = 'pipeline'
        def project_pipeline_dir = "${pipeline_dir}/ms-pipeline"
        def service_pipeline_dir = "${project_pipeline_dir}/${this.service_name}"
        // def docker_repo = "registry-intl-vpc.ap-southeast-5.aliyuncs.com/investree/"
        def docker_repo = "registry-intl.ap-southeast-5.aliyuncs.com/dekribellyliu/"
        
        def docker_image_name = ((env == "development") ? "${this.kube_namespace}-" : "" ) + this.service_name
        if (this.kube_namespace == "staging") {
            docker_image_name = "staging-"+docker_image_name
        }

        def service_url = [
            development : "svc.investree.id/validate/${this.service_name}/",
            staging : ((this.kube_namespace == "staging") ? "svc.investree.tech" : "stg-svc.investree.id") + "/validate/${this.service_name}/",
            production : "svc.investree.id/validate/${this.service_name}/",
        ]

        return [
            service_name: this.service_name,
            service_branch: this.service_branch,
            version_tag: this.version_tag,
            slack_channel: this.slack_channel,
            pipeline_dir: pipeline_dir,
            project_pipeline_dir: project_pipeline_dir,
            dockerfile: "${project_pipeline_dir}/dockerfile",
            bootstrap: "${project_pipeline_dir}/bootstrap.yml",
            env_configmap: "${pipeline_dir}/env-vars-configmap/${this.env}"+ ((this.kube_namespace == "staging") ? "2" : "") +"-configmap.yaml",
            service_configmap: "${service_pipeline_dir}/configmap.yaml",
            service_cors: "${service_pipeline_dir}/cors",
            docker_image: docker_repo + docker_image_name,
            // docker_registry_cred_id: "k8s-deployer",
            // docker_registry_cred_id: "dockerhub",
            docker_registry_cred_id: "my-acr",
            kube_cred_id: this.kubeCredential[env].kube_cred_id,
            kube_api_server: this.kubeCredential[env].kube_api_server,
            kube_namespace: this.kube_namespace,
            kube_deployment_name: "${this.envAbbrev[env]}-${this.service_name}",
            kube_set_replica: ((env == "development") ? "1" : "3"),
            service_url: ((env == "development") ? "https://${this.kube_namespace}-" : "https://" ) + service_url[this.env],
            logFilePath: "${this.envAbbrev[env]}-${this.service_name}-${this.kube_namespace}.log",
            botCredentialId: "bot-slack-log-notif"
        ]
    }

    def buildGeneral() {

        def base_ms_pipeline = baseMsPipeline()

        return base_ms_pipeline + [
            kube_deployment_file: "${base_ms_pipeline.project_pipeline_dir}/kube-deployment.yaml",
            kube_deployment_belly: "${base_ms_pipeline.project_pipeline_dir}/kube-deployment-belly.yaml"
        ]

    }

    def buildAuth() {
        def base_ms_pipeline = baseMsPipeline()
        return base_ms_pipeline + [
            kube_deployment_file: "${base_ms_pipeline.project_pipeline_dir}/kube-deployment-auth-service.yaml",
            kube_deployment_file_patch : "${base_ms_pipeline.project_pipeline_dir}/kube-deployment-auth-service-patch.yaml"
        ]
    }

    def buildStorage(){
        def base_ms_pipeline = baseMsPipeline()
        return base_ms_pipeline + [
            kube_deployment_file: "${base_ms_pipeline.project_pipeline_dir}/kube-deployment-storage.yaml"
        ]
    }

    def buildNginxProxy() {
        def pipeline_dir = 'pipeline'
        def service_pipeline_dir = "${pipeline_dir}/${this.service_name}"
        // def docker_repo = "registry-intl-vpc.ap-southeast-5.aliyuncs.com/investree/"
        def docker_repo = "registry-intl.ap-southeast-5.aliyuncs.com/dekribellyliu/" 
        def docker_image_name = ((env == "development") ? "${this.kube_namespace}-" : "" ) + this.service_name
        if (this.kube_namespace == "staging") {
            docker_image_name = "staging-"+docker_image_name
        }

        def service_url = [
            development : ".investree.id",
            staging : (this.kube_namespace == "staging") ? "investree.tech" : "staging.investree.id",
            production : "investree.id",
        ]

        return [
            service_name: this.service_name,
            service_branch: this.service_branch,
            // slack_channel: this.slack_channel,
            pipeline_dir: pipeline_dir,
            dockerfile: "${service_pipeline_dir}/dockerfile",
            env_configmap: "${pipeline_dir}/env-vars-configmap/${this.env}"+ ((this.kube_namespace == "staging") ? "2" : "" ) + "-configmap.yaml",
            ingressfile: "${service_pipeline_dir}/ingress/${this.env}"+ ((this.kube_namespace == "staging") ? "2" : "") + ".investree.id.yaml",
            docker_image: docker_repo + docker_image_name,
            docker_registry_cred_id: "k8s-deployer",
            kube_cred_id: this.kubeCredential[env].kube_cred_id,
            kube_api_server: this.kubeCredential[env].kube_api_server,
            kube_namespace: this.kube_namespace,
            kube_deployment_name: "${this.envAbbrev[env]}-${this.service_name}",
            kube_deployment_file: "${service_pipeline_dir}/kube-deployment.yaml",
            kube_set_replica: ((env == "development") ? "1" : "2" ),
            service_url: ((env == "development") ? "https:/${this.kube_namespace}" : "https://") + service_url[this.env]
        ]
    }

}