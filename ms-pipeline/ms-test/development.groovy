/* Version 3.0
   DEVELOP BRANCH PIPELINE
*/

jsl = library('pipeline-library@master')

msPipelineDev(
  jsl: jsl,
  service_name: MS_NAME,
  kube_namespace: MS_NAMESPACE,
  service_branch: MS_BRANCH,
  slack_channel: SLACK_CHANNEL
)